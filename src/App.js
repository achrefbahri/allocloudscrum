import React, { Component } from "react";
import Navbar from "./Navbar";
import Column from "./Column";
import { DragDropContext } from "react-beautiful-dnd";
import data from "./data";
import "./app.css";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = data;
  }

  onDragEnd = res => {
 
    const { destination, source } = res;
    if (!destination) return;
    let newData = this.state;

    let indexColumnStart;
    let indexColumnEnd;
    for (let i = 0; i < newData.columns.length; i++) {
      if (newData.columns[i].id === source.droppableId) indexColumnStart = i;
      if (newData.columns[i].id === destination.droppableId) indexColumnEnd = i;
    }
    if (destination.droppableId === source.droppableId) {
      const sourceTask =
        newData.columns[indexColumnStart].tasksId[source.index];
      newData.columns[indexColumnStart].tasksId[source.index] =
        newData.columns[indexColumnEnd].tasksId[destination.index];

      newData.columns[indexColumnEnd].tasksId[destination.index] = sourceTask;

      this.setState({ columns: newData.columns });
      return;
    }
    const sourceTask = newData.columns[indexColumnStart].tasksId[source.index];
    newData.columns[indexColumnStart].tasksId.splice(source.index, 1);
    newData.columns[indexColumnEnd].tasksId.splice(source.index, 0, sourceTask);
    this.setState({ columns: newData.columns });
  };

  render() {
    const data = this.state;
    return (
      <div className="App">
        <Navbar />
        <DragDropContext onDragEnd={this.onDragEnd}>
          <div className="Container">
            {data.columns.map(column => {
              const tasks = column.tasksId.map(taskId => data.tasks[taskId]);
              return (
                <div key={column.id} className="Columns">
                  <Column column={column} tasks={tasks} />
                </div>
              );
            })}
          </div>
        </DragDropContext>
      </div>
    );
  }
}

export default App;
