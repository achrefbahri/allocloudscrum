const data = {
  columns: [
    { title: "To do", id: "column1", tasksId: ["task1", "task2", "task3"] },
    { title: "Doing", id: "column2", tasksId: ["task4", "task5", "task6"] },
    { title: "Done", id: "column3", tasksId: ["task7", "task8", "task9"] }
  ],
  tasks: {
    task1: {
      id: "task1",
      taskContent: " Excepteur sint occaeca"
    },
    task2: {
      id: "task2",
      taskContent: "minim veniam, quis nostrud exercitation ullamco laboris n"
    },
    task3: {
      id: "task3",
      taskContent: "ullam corporis suscipit laboriosam, ni"
    },
    task4: {
      id: "task4",
      taskContent:
        "eum iure reprehenderit qui in ea voluptate velit esse quam nihil"
    },
    task5: {
      id: "task5",
      taskContent: "et dolore magnam aliquam quaerat voluptatem."
    },
    task6: {
      id: "task6",
      taskContent: " Ut enim ad minima veniam, quis nostrum exercitationem "
    },
    task7: {
      id: "task7",
      taskContent:
        "pisci velit, sed quia non numquam eius modi tempora incidunt ut labore Quis autem vel  "
    },
    task8: {
      id: "task8",
      taskContent:
        "laudantium, totam rem       aperiam, eaque ipsa quae ab illo inventore"
    },
    task9: {
      id: "task9",
      taskContent: "Sed ut perspiciatis unde omnis iste natus "
    }
  }
};

export default data;
