import React, { Component } from "react";
import Task from "./Task";
import { Droppable } from "react-beautiful-dnd";

class Column extends Component {
  render() {
    const { column, tasks } = this.props;
    return (
      <div className="container">
        <h3>{column.title}</h3>
         <Droppable droppableId={column.id}>
          {(provided, snapshot) => (
            <div
              className="Column"
              ref={provided.innerRef}
              {...provided.droppableProps}
              style={{
                backgroundColor: snapshot.isDraggingOver ? "skyblue" : "white"
              }}
            >

              {tasks.map((task, index) => (
                <Task
                  key={task.id}
                  id={task.id}
                  taskContent={task.taskContent}
                  index={index}
                />
              ))}
                            {provided.placeholder}

            </div>
          )}
        </Droppable>
      </div>
    );
  }
}

export default Column;
