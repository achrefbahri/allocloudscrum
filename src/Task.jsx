import React from "react";
import { Draggable } from "react-beautiful-dnd";

const Task = ({ taskContent, id, index }) => {
  return (
    <Draggable draggableId={id} index={index}>
      {(provided) => (
        <div
          className="Task"
          {...provided.draggableProps}
          {...provided.dragHandleProps}
          ref={provided.innerRef}
 
        >
          {taskContent}
         
 
        </div>
      )}
    </Draggable>
  );
};

export default Task;
